import { Mode, plugin as markdownPlugin } from 'vite-plugin-markdown';
import { sveltekit } from '@sveltejs/kit/vite';
import houdini from 'houdini/vite';
import MarkdownIt from './src/lib/util/markdown-it';

/** @type {import('vite').UserConfig} */
export default {
  server: {
    port: 3000,
    fs: {
      allow: ['./static']
    }
  },
  plugins: [
    houdini(),
    sveltekit(),
    markdownPlugin({
      mode: [Mode.TOC, Mode.HTML],
      markdownIt: MarkdownIt()
    })
  ]
};
