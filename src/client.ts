import { HoudiniClient, ClientPlugin } from '$houdini';
import { subscription } from '$houdini/plugins';
import { authToken } from '$stores/authStore';
import { createClient } from 'graphql-ws';
import { get } from 'svelte/store';
import Env from '$lib/Env';

const keycloakPlugin: ClientPlugin = () => {
  return {
    async beforeNetwork(ctx, { next }) {
      let result;
      try {
        result = get(authToken);
      } catch (e) {
        console.warn(`error getting token: ${e}`);
        return next(ctx);
      }
      if (result == null) {
        console.debug('no token available, not adding to headers');
        return next(ctx);
      }

      // Set session token for use in other plugins (like graphql-ws)
      if (!ctx.session) {
        console.debug("unsure why session is null, can't set session token");
      } else {
        ctx.session.token = result;
      }

      if (ctx.fetchParams == null) {
        console.debug("unsure why fetchParams is null, can't set Authorization header");
        return next(ctx);
      }
      // Enable to print JWT
      // console.log(result)
      // TODO: Figure out if this is overwriting all headers?
      const headers: Record<string, string> = {
        Authorization: `Bearer ${result}`
      };
      if (ctx.metadata?.role) {
        headers['x-hasura-role'] = ctx.metadata.role;
      }
      ctx.fetchParams.headers = headers;
      // Enable to print queries and inputs
      // console.log(ctx.text)
      // console.log(ctx.variables)
      next(ctx);
    }
  };
};

export default new HoudiniClient({
  url: Env.GraphQLUrl,
  plugins: [
    keycloakPlugin,
    subscription(({ session }) =>
      createClient({
        url: Env.GraphQLUrl.replace('http', 'ws'),
        connectionParams: () => {
          return {
            headers: {
              // todo handle roles
              Authorization: `Bearer ${session?.token}`
            }
          };
        }
      })
    )
  ]
});
