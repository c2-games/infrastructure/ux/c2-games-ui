import type { PageLoad } from './$types';

export const load: PageLoad = ({ params }) => {
  return { banner: { title: 'NCAE Cyber Games 2025 Schedule', binary: true } };
};
