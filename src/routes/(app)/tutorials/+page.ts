import type { PageLoad } from './$types';

export const load: PageLoad = ({ params }) => {
  return { banner: { title: 'Sandbox Tutorial Videos', binary: true } };
};
