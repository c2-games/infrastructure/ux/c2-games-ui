import type { PageLoad } from './$types';

export const load: PageLoad = ({ params }) => {
  return { banner: { title: 'Become a sponsor of the NCAE Cybergames' } };
};
