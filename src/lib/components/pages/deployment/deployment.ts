import { parse } from 'yaml';

export type InputFormat = {
  event: {
    vars: Record<string, unknown>;
    scoring_periods: Array<{
      // offsets in hour:minute format
      start_offset: string;
      end_offset: string;
    }>;
    points: {
      total_points: number;
      scoring_interval: number;
    };
  };
  challenges: Array<{
    name: string;
    points: number;
    unlock_delay: number; // minutes from event start
    flags: string[];
  }>;
  checks: Array<{
    name: string;
    description: string;
    display_order: number;
    display_name: string;
    subnet: string;
    host: string;
    scoring_weight?: number;
    script: string;
    args?: string;
    vars?: string;
    type: string;
  }>;
  vars: {
    [key: string]: Record<string, unknown>;
  };
};

export async function parseFile(file: File): Promise<InputFormat> {
  console.log('parsing file');
  const text = await file.text();
  const parsed = parse(text) as InputFormat;
  if (!parsed.checks) {
    throw new Error('No checks found in file');
  }
  if (!parsed.vars) {
    throw new Error('No event found in file');
  }

  console.log('parsed', parsed);
  return parsed;
}
