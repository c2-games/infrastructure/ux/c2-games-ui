export default [
  {
    title: '1: Intro to the Environment',
    category: 'Intro to Linux Admin',
    href: 'https://www.youtube.com/embed?v=hL9pTiRNPqE&amp;list=PLqux0fXsj7x3WYm6ZWuJnGC1rXQZ1018M&amp;index=1'
  },
  {
    title: '2: Linux Commands and File Structure',
    category: 'Intro to Linux Admin',
    href: 'https://www.youtube.com/embed?v=N9j--n-zGgc&amp;list=PLqux0fXsj7x3WYm6ZWuJnGC1rXQZ1018M&amp;index=2'
  },
  {
    title: '3: File System Navigation from the Terminal',
    category: 'Intro to Linux Admin',
    href: 'https://www.youtube.com/embed?v=lI0mUMqBesU&amp;list=PLqux0fXsj7x3WYm6ZWuJnGC1rXQZ1018M&amp;index=3'
  },
  {
    title: '4: Making Directories and Understanding File Paths',
    category: 'Intro to Linux Admin',
    href: 'https://www.youtube.com/embed?v=7JYJO_D8zVs&amp;list=PLqux0fXsj7x3WYm6ZWuJnGC1rXQZ1018M&amp;index=4'
  },
  {
    title: '5: Moving and Copying',
    category: 'Intro to Linux Admin',
    href: 'https://www.youtube.com/embed?v=gSVg40u0fZE&amp;list=PLqux0fXsj7x3WYm6ZWuJnGC1rXQZ1018M&amp;index=5'
  },
  {
    title: '6: Removing Directories and Using the History',
    category: 'Intro to Linux Admin',
    href: 'https://www.youtube.com/embed?v=twREXouRxns&amp;list=PLqux0fXsj7x3WYm6ZWuJnGC1rXQZ1018M&amp;index=6'
  },
  {
    title: '7: Creating and Reading Files',
    category: 'Intro to Linux Admin',
    href: 'https://www.youtube.com/embed?v=2DcDQe8idtU&amp;list=PLqux0fXsj7x3WYm6ZWuJnGC1rXQZ1018M&amp;index=7'
  },
  {
    title: '8: Editing with Nano or Vi',
    category: 'Intro to Linux Admin',
    href: 'https://www.youtube.com/embed?v=rR_n2ciilrc&amp;list=PLqux0fXsj7x3WYm6ZWuJnGC1rXQZ1018M&amp;index=8'
  },
  {
    title: '9: Creating Files with Editors and Deleting Files',
    category: 'Intro to Linux Admin',
    href: 'https://www.youtube.com/embed?v=l0d7ks9ZkjU&amp;list=PLqux0fXsj7x3WYm6ZWuJnGC1rXQZ1018M&amp;index=9'
  },
  {
    title: '10: Creating User Accounts',
    category: 'Intro to Linux Admin',
    href: 'https://www.youtube.com/embed?v=y6-e233rrQE&amp;list=PLqux0fXsj7x3WYm6ZWuJnGC1rXQZ1018M&amp;index=10'
  },
  {
    title: '11: Managing Permissions and Sudo Users',
    category: 'Intro to Linux Admin',
    href: 'https://www.youtube.com/embed?v=to0GrfGERK0&amp;list=PLqux0fXsj7x3WYm6ZWuJnGC1rXQZ1018M&amp;index=11'
  },
  {
    title: '12: Exploring Sudoers and Removing Users',
    category: 'Intro to Linux Admin',
    href: 'https://www.youtube.com/embed?v=7IsIjTBK7kk&amp;list=PLqux0fXsj7x3WYm6ZWuJnGC1rXQZ1018M&amp;index=12'
  },
  {
    title: '13: Groups',
    category: 'Intro to Linux Admin',
    href: 'https://www.youtube.com/embed?v=BJJBSUe5JEk&amp;list=PLqux0fXsj7x3WYm6ZWuJnGC1rXQZ1018M&amp;index=13'
  },
  {
    title: '14: Passwords and Shadow Hashes',
    category: 'Intro to Linux Admin',
    href: 'https://www.youtube.com/embed?v=P-BJP9wVH_U&amp;list=PLqux0fXsj7x3WYm6ZWuJnGC1rXQZ1018M&amp;index=14'
  },
  {
    title: '15: Less Grep and Pipe',
    category: 'Intro to Linux Admin',
    href: 'https://www.youtube.com/embed?v=2neT3phfYts&amp;list=PLqux0fXsj7x3WYm6ZWuJnGC1rXQZ1018M&amp;index=15'
  },
  {
    title: '16: Background on Services',
    category: 'Intro to Networking',
    href: 'https://www.youtube.com/embed?v=-FjEEoLUb64&amp;list=PLqux0fXsj7x3WYm6ZWuJnGC1rXQZ1018M&amp;index=16'
  },
  {
    title: '17: Exploring Networking Configuration',
    category: 'Intro to Networking',
    href: 'https://www.youtube.com/embed?v=CfLYrLpsUsQ&amp;list=PLqux0fXsj7x3WYm6ZWuJnGC1rXQZ1018M&amp;index=17'
  },
  {
    title: '18: Static Configuration in Kali with Interfaces File',
    category: 'Intro to Networking',
    href: 'https://www.youtube.com/embed?v=6AG68GIMw5o&amp;list=PLqux0fXsj7x3WYm6ZWuJnGC1rXQZ1018M&amp;index=18'
  },
  {
    title: '19: Static Configuration in CentOS with ifcfg Files',
    category: 'Intro to Networking',
    href: 'https://www.youtube.com/embed?v=fQPJNI6zI8g&amp;list=PLqux0fXsj7x3WYm6ZWuJnGC1rXQZ1018M&amp;index=19'
  },
  {
    title: '20: Static Configuration in Ubuntu with Netplan',
    category: 'Intro to Networking',
    href: 'https://www.youtube.com/embed?v=3vbFNmnJCl0&amp;list=PLqux0fXsj7x3WYm6ZWuJnGC1rXQZ1018M&amp;index=20'
  },
  {
    title: '21: Testing Connectivity with Ping',
    category: 'Intro to Networking',
    href: 'https://www.youtube.com/embed?v=DROCT3-hU3c&amp;list=PLqux0fXsj7x3WYm6ZWuJnGC1rXQZ1018M&amp;index=21'
  },
  {
    title: '22: Temporary Permanent and Flushing IPs',
    category: 'Intro to Networking',
    href: 'https://www.youtube.com/embed?v=von08e7PlWk&amp;list=PLqux0fXsj7x3WYm6ZWuJnGC1rXQZ1018M&amp;index=22'
  },
  {
    title: '23: Using Networking Service with Netcat',
    category: 'Intro to Networking',
    href: 'https://www.youtube.com/embed?v=_aIac8hweAg&amp;list=PLqux0fXsj7x3WYm6ZWuJnGC1rXQZ1018M&amp;index=23'
  },
  {
    title: '24: Web Services with Apache',
    category: 'Routing and Services',
    href: 'https://www.youtube.com/embed?v=PFvFsiLSu94&amp;list=PLqux0fXsj7x3WYm6ZWuJnGC1rXQZ1018M&amp;index=24'
  },
  {
    title: '25: Router Configuration and Mini Hack Completion',
    category: 'Routing and Services',
    href: 'https://www.youtube.com/embed?v=nm680gA5dXQ&amp;list=PLqux0fXsj7x3WYm6ZWuJnGC1rXQZ1018M&amp;index=25'
  },
  {
    title: '26: Mini Hack Reconfiguration',
    category: 'Routing and Services',
    href: 'https://youtube.com/embed/AujsQji3A1Q'
  },
  {
    title: '27: SSH Service Basics',
    category: 'Routing and Services',
    href: 'https://youtube.com/embed/krqJhN4Ld6s'
  },
  {
    title: '28: SSH Service Cryptographic Keys',
    category: 'Routing and Services',
    href: 'https://youtube.com/embed/GivTVjSUjRM'
  },
  {
    title: '29: SSH Service Passwordless Authentication',
    category: 'Routing and Services',
    href: 'https://youtube.com/embed/MBmWgXb73gE'
  },
  {
    title: '30: SSH Service Through the Router',
    category: 'Routing and Services',
    href: 'https://youtube.com/embed/NzitxTqu2o0'
  },
  {
    title: '31: DNS Service Basics',
    category: 'Routing and Services',
    href: 'https://youtube.com/embed/mpVX4jxF4ww'
  },
  {
    title: '32: DNS Service Additional Zones',
    category: 'Routing and Services',
    href: 'https://youtube.com/embed/y2-w9m4yvqw'
  },
  {
    title: '33: DNS Service Through the Router',
    category: 'Routing and Services',
    href: 'https://youtube.com/embed/AdPaZOU-ngM'
  },
  {
    title: '34: Rsync Service Basics',
    category: 'Routing and Services',
    href: 'https://www.youtube.com/embed?v=nm680gA5dXQ&amp;list=PLqux0fXsj7x3WYm6ZWuJnGC1rXQZ1018M&amp;index=34'
  },
  {
    title: '35: Cron Service Intro',
    category: 'Routing and Services',
    href: 'https://www.youtube.com/embed?v=nm680gA5dXQ&amp;list=PLqux0fXsj7x3WYm6ZWuJnGC1rXQZ1018M&amp;index=35'
  },
  {
    title: '36: Rsync and Cron Automatic Secure Backups',
    category: 'Routing and Services',
    href: 'https://www.youtube.com/embed?v=nm680gA5dXQ&amp;list=PLqux0fXsj7x3WYm6ZWuJnGC1rXQZ1018M&amp;index=36'
  },
  {
    title: '37: Intro to Firewalls with UFW',
    category: 'Routing and Services',
    href: 'https://www.youtube.com/embed?v=nm680gA5dXQ&amp;list=PLqux0fXsj7x3WYm6ZWuJnGC1rXQZ1018M&amp;index=37'
  },
  {
    title: '38: Active Connection Defense',
    category: 'Routing and Services',
    href: 'https://www.youtube.com/embed?v=nm680gA5dXQ&amp;list=PLqux0fXsj7x3WYm6ZWuJnGC1rXQZ1018M&amp;index=38'
  },
  {
    title: '39: Scripting Hello World',
    category: 'Intro to Scripting',
    href: 'https://www.youtube.com/embed?v=KOKZgzli_0k&amp;list=PLqux0fXsj7x3WYm6ZWuJnGC1rXQZ1018M&amp;index=39'
  },
  {
    title: '40: Variables and User Input',
    category: 'Intro to Scripting',
    href: 'https://www.youtube.com/embed?v=R91aqAQRrDk&amp;list=PLqux0fXsj7x3WYm6ZWuJnGC1rXQZ1018M&amp;index=40'
  },
  {
    title: '41: Scripts and File Paths',
    category: 'Intro to Scripting',
    href: 'https://www.youtube.com/embed?v=AfOx5qUTHvQ&amp;list=PLqux0fXsj7x3WYm6ZWuJnGC1rXQZ1018M&amp;index=41'
  },
  {
    title: '42: Scripting and Root Permissions',
    category: 'Intro to Scripting',
    href: 'https://www.youtube.com/embed?v=48QKX_zhdno&amp;list=PLqux0fXsj7x3WYm6ZWuJnGC1rXQZ1018M&amp;index=42'
  },
  {
    title: '43: Scripted IP Changer Low Sophistication',
    category: 'Intro to Scripting',
    href: 'https://www.youtube.com/embed?v=gtAH-GuPgZ8&amp;list=PLqux0fXsj7x3WYm6ZWuJnGC1rXQZ1018M&amp;index=43'
  },
  {
    title: '44: MikroTik Router and Mini Hack Completion',
    category: 'Routing and Services',
    href: 'https://www.youtube.com/embed?v=gu5A2yCITRs&amp;list=PLqux0fXsj7x3WYm6ZWuJnGC1rXQZ1018M&amp;index=44'
  }
];
