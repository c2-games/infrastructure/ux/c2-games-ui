export default [
  {
    region: 'North Eastern',
    contacts: [
      {
        name: ' Michael Qaissaunee',
        email: 'mqaissaunee@brookdalecc.edu'
      }
    ]
  },
  {
    region: 'South Eastern',
    contacts: [
      {
        name: 'Anthony Pinto',
        email: 'apinto@uwf.edu'
      }
    ]
  },
  {
    region: 'Mid Western',
    contacts: [
      {
        name: 'Lonnie Decker',
        email: 'mdldecker@davenport.edu'
      }
    ]
  },
  {
    region: 'South Western',
    contacts: [
      {
        name: 'Stu Steiner',
        email: 'ssteiner@ewu.edu'
      }
    ]
  },
  {
    region: 'North Western',
    contacts: [
      {
        name: 'Albert Tay',
        email: 'albert_tay@byu.edu'
      }
    ]
  }
];
