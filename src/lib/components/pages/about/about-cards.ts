export default [
  {
    background: "bg-[url('$lib/assets/images/about/resume.png')]",
    title: 'It Looks Great On Your Resume',
    text: 'Employers view competitions as proof that you have hands on keyboard experience and know how to work as part of team.'
  },
  {
    background: "bg-[url('$lib/assets/images/about/helps_you_find.png')]",
    title: 'It Helps You Find Your Place In The Field',
    text: 'Competitions give you a chance to try out the different job roles and tasks in the field so you can start getting a feel for the roles that you find motivating and fulfilling.'
  },
  {
    background: "bg-[url('$lib/assets/images/about/build_your_network.png')]",
    title: 'It Helps Build Your Network',
    text: 'Employers pay attention to collegiate cyber competitions, often sending recruiters to events to scout new talent. Competitions are also a great way to connect with other students who are interested in the field to start building a network of future professional colleagues.'
  },
  {
    background: "bg-[url('$lib/assets/images/about/award.png')]",
    title: 'Earn Cool Rewards',
    text: 'Tell your story and show off your achievements with awards and prizes!'
  }
];
