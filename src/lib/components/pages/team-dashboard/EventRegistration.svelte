<script lang="ts">
  import { ParticipantEventsByGroupStore, RegisterTeamStore, UnregisterTeamStore } from '$houdini';
  import HeaderWithBackdrop from '$lib/components/ui-elements/HeaderWithBackdrop.svelte';
  import { faCircleCheck, faLock, faUserGroup, faUserMinus } from '@fortawesome/free-solid-svg-icons';
  import Fa from 'svelte-fa';
  import { onMount } from 'svelte';
  import TeamChecklistItem from '$lib/components/pages/team-dashboard/TeamChecklistItem.svelte';

  import { getNotificationsContext } from 'svelte-notifications';
  import { authTokenParsed } from '$stores/authStore';
  import type { Event, EventGroup, EventGroupWithChecklists, EventWithChecklist, Team } from '$lib/graphql/types';
  import type { ChecklistItem } from '$lib/interfaces/event-registration';
  import registrationBg from '$lib/assets/images/home/collaborative.png';
  import { formatToLocalTime } from '$lib/util/timeHelpers';
  import WelcomeGuideAlert from '$lib/components/ui-elements/WelcomeGuideAlert.svelte';

  const { addNotification } = getNotificationsContext();

  export let team: Team;

  let eventGroupsStore = new ParticipantEventsByGroupStore();
  $: eventGroupsStore.subscribe((result) => {
    if (result.errors?.length) console.error('failed fetch', result.errors);
  });

  $: console.log('eventGroups', eventGroups);

  let eventGroups: EventGroupWithChecklists[] = [];

  async function loadEvents() {
    await eventGroupsStore.fetch({ variables: { starts_after: new Date().toISOString() } });
    if ($eventGroupsStore.data?.event_groups) {
      let tmp = <EventGroupWithChecklists[]>$eventGroupsStore.data.event_groups;
      for (const eventGroup of tmp) {
        for (const event of eventGroup.events) {
          event.checklist = generateChecklist(team, event, eventGroup);
        }

        // Sort events so that the most eligible events are on top
        eventGroup.events.sort((a: EventWithChecklist, b: EventWithChecklist) => {
          const aCompleted = a.checklist?.filter((c) => c.completed).length || 0;
          const bCompleted = b.checklist?.filter((c) => c.completed).length || 0;
          return bCompleted - aCompleted;
        });
      }

      eventGroups = tmp;
    } else console.warn('failed to fetch event groups');
  }

  onMount(loadEvents);

  async function registerTeam(team: Team, event: Event) {
    const result = await new RegisterTeamStore().mutate(
      { event_id: event.id, team_id: team.id },
      { metadata: { role: 'participant' } }
    );

    if (result.errors?.length) {
      for (const e of result.errors) {
        addNotification({
          text: e.message,
          position: 'bottom-right',
          removeAfter: 5000,
          type: 'error'
        });
      }
      throw new Error(result.errors.map((e) => e.message).join('\n'));
    }

    await loadEvents();
    addNotification({
      text: `Successfully registered team for ${event.name}`,
      position: 'bottom-right',
      removeAfter: 5000,
      type: 'success'
    });
  }

  async function unregisterTeam(team: Team, event: Event) {
    const result = await new UnregisterTeamStore().mutate(
      { event_id: event.id, team_id: team.id },
      { metadata: { role: 'participant' } }
    );

    if (result.errors) {
      for (const e of result.errors) {
        addNotification({
          text: e.message,
          position: 'bottom-right',
          removeAfter: 5000,
          type: 'error'
        });
      }
      throw new Error(result.errors.map((e) => e.message).join('\n'));
    }

    await loadEvents();
    addNotification({
      text: `Team is no longer registered for ${event.name}`,
      position: 'bottom-right',
      removeAfter: 5000,
      type: 'success'
    });
  }

  function teamRegisteredForEvent(team: Team, event: Event, eventGroup: EventGroup): boolean {
    const ev = eventGroup.events.find((e) => e.id === event.id);

    if (!ev) {
      console.debug('event not found in event group');
      return false;
    }

    return !!ev.teams.find((t) => t.team_id === team.id);
  }

  function calculateEventDuration(start: Date | null | undefined, end: Date | null | undefined): string {
    if (!start || !end) {
      return 'Invalid date';
    }

    const duration = (end.getTime() - start.getTime()) / (1000 * 60 * 60); // in hours
    return `${duration} hours`;
  }

  // Utility function to calculate time until a specific date
  function calculateTimeUntil(dateString: Date | null | undefined): string | null | undefined {
    if (!dateString) {
      return undefined;
    }
    const now = new Date();
    const timeUntilMs = dateString.getTime() - now.getTime(); // in milliseconds

    if (timeUntilMs < 0) {
      return null;
    }

    const timeUntilHours = timeUntilMs / (1000 * 60 * 60); // in hours
    const days = Math.floor(timeUntilHours / 24);
    const hours = Math.round(timeUntilHours % 24);

    if (days > 0) {
      return `${days} days ${hours} hours`;
    } else if (hours > 0) {
      return `${hours} hours`;
    } else {
      const minutes = Math.round((timeUntilMs % (1000 * 60 * 60)) / (1000 * 60));
      return `${minutes} minutes`;
    }
  }

  function generateChecklist(team: Team, event: Event, eventGroup: EventGroup): ChecklistItem[] {
    // These checks are confirmed server-side during the mutation
    // TODO perform UI checks server side to consolidate logic
    const checklist: ChecklistItem[] = [];

    const linked = team.members.filter(u => u.user!.discord_linked)
    checklist.push({
      name: 'Discord Linked',
      completed: linked.length === team.members.length,
      status: 'warning', // default to warning status if not completed
      description: `All team members have linked their Discord accounts prior to registration deadline.`
    });

    // Check how many teams are allowed to register vs how many are already registered
    if (eventGroup.max_teams_per_event) {
      checklist.push({
        name: 'Team Limit',
        completed: event.teams.length < eventGroup.max_teams_per_event,
        status: "error", // error if not complete
        description: `This event has ${event.teams.length} / ${eventGroup.max_teams_per_event} registered teams.`
      });
    }

    if (event.event_registration_setting?.min_team_size) {
      checklist.push({
        name: 'Minimum Team Size',
        completed: team.members.length >= event.event_registration_setting.min_team_size,
        status: "error", // error if not complete
        description: `This event requires a minimum of ${event.event_registration_setting.min_team_size} team members.`
      });
    }

    if (event.event_registration_setting?.max_team_size) {
      checklist.push({
        name: 'Maximum Team Size',
        completed: team.members.length <= event.event_registration_setting.max_team_size,
        status: "error", // error if not complete
        description: `This event requires a maximum of ${event.event_registration_setting.max_team_size} team members.`
      });
    }

    if (event.event_registration_setting?.require_liaison) {
      checklist.push({
        name: 'Faculty Liaison',
        completed: !!team.liaison_id,
        status: "error", // error if not complete
        description: `This event requires a Faculty Liaison from your institution to be registered.`
      });
    }

    // Check if the regions available to this institution can register for this event
    if (event.event_registration_setting?.region_id) {
      const checklistItem: ChecklistItem = {
        name: 'Region',
        completed: false,
        status: "error", // error if not complete
        description: `This event is only available to teams in the ${event.event_registration_setting?.region?.name} region. `
      };
      if (team.institution?.regions) {
        checklistItem.completed = team.institution.regions
          // todo is this filter masking a bug?
          .filter((r) => r.region_id)
          .map((r) => r.region_id)
          .includes(event.event_registration_setting.region_id);
      } else {
        checklistItem.description +=
          'Your institution is not assigned a region. Please reach out to support@c2games.org';
      }

      checklist.push(checklistItem);
    }

    // Check for institutions registered to other events in the event group
    if (eventGroup.institution_teams_same_event) {
      let institutionEvents = [];
      for (const ev of eventGroup.events) {
        if (ev.id === event.id) {
          continue;
        }

        if (ev.teams.find((t) => t.team.institution_id === team.institution_id)) {
          institutionEvents.push(ev);
        }
      }

      if (institutionEvents.length > 0) {
        checklist.push({
          name: 'Institution Teams in Multiple Events',
          completed: false,
          status: "error", // error if not complete
          description:
            `Your institution has already registered for ${institutionEvents.length} other events ` +
            `in this competition: ${institutionEvents.map((ev) => ev.name).join(', ')}`
        });
      } else {
        checklist.push({
          name: 'Institution Teams in Multiple Events',
          completed: true,
          status: "error", // error if not complete
          description: `Your institution has not registered for any other events in this competition.`
        });
      }
    }

    if (eventGroup.max_institution_teams_per_event !== null) {
      // Check for max institution teams
      const institutionRegistrations = eventGroup.events.reduce((acc, ev) => {
        return acc + ev.teams.filter((t) => t.team?.institution_id === team.institution_id).length;
      }, 0);
      console.log('institutionRegistrations', institutionRegistrations);

      checklist.push({
        name: 'Institution Team Limit',
        completed: institutionRegistrations < eventGroup.max_institution_teams_per_event,
        status: "error", // error if not complete
        description:
          `Each Institution may only participate with ${eventGroup.max_institution_teams_per_event} ` +
          `teams in this competition.`
      });
    }

    return checklist;
  }

  function registrationActive(event: Event): boolean {
    if (event.event_registration_setting && event.event_registration_setting.registration_active !== null) {
      return event.event_registration_setting.registration_active;
    }
    return true;
  }
</script>

<HeaderWithBackdrop size="sm" secondary>Event Registration</HeaderWithBackdrop>

{#each eventGroups as eventGroup}
  {#if eventGroup.events.length > 0}
    <div class="relative flex pt-5 items-center">
      <span class="flex-shrink text-2xl text-gray-500 px-4 italic font-light">{eventGroup.name}</span>
      <div class="flex-grow border-t border-gray-400" />
    </div>
  {/if}
  {#each eventGroup.events as event}
    <!-- todo: add filter for "my region", or display those at the top of the list? -->
    <div class="w-full sm:w-full lg:max-w-full lg:flex p-5">
      <div
        class="h-48 lg:h-auto lg:w-48 flex-none bg-cover rounded-t lg:rounded-t-none lg:rounded-l text-center overflow-hidden"
        style="background-image: url({registrationBg})" />
      <div
        class="border-r border-b border-l border-gray-400 lg:border-l-0 lg:border-t lg:border-gray-400 bg-white rounded-b lg:rounded-b-none lg:rounded-r p-4 flex flex-col justify-between leading-normal grow">
        <div class="mb-8">
          {#if teamRegisteredForEvent(team, event, eventGroup)}
            <p class="text-sm text-green-500 flex items-center">
              <Fa class="inline-block mr-2" color="#22c55e" icon={faCircleCheck} />

              Registered!
            </p>
          {/if}
          <div class="text-gray-900 font-bold text-xl mb-2">{event.name} - {eventGroup.name}</div>
          {#if event.event_registration_setting}
            {#if event.event_registration_setting.region_id}
              <div class="text-gray-900 font-bold text-l mb-2 ">
                Region - {event.event_registration_setting.region?.name}
              </div>
            {/if}
            <p><b>Event Start:</b> {formatToLocalTime(event.start)}</p>
            <p><b>Duration:</b> {calculateEventDuration(event.start, event.end)}</p>
            {#if calculateTimeUntil(event.event_registration_setting.registration_open)}
              <p>
                <b>Registration Begins:</b>
                {calculateTimeUntil(event.event_registration_setting.registration_open)}
              </p>
            {/if}
            {#if calculateTimeUntil(event.event_registration_setting.registration_close)}
              <p>
                <b>Registration Ends:</b>
                {calculateTimeUntil(event.event_registration_setting.registration_close)}
              </p>
            {/if}

            <p>
              <b>Roster Change Ends</b>
              {calculateTimeUntil(event.event_registration_setting.roster_change_close)}
            </p>
          {/if}
        </div>

        <div>
          {#if teamRegisteredForEvent(team, event, eventGroup)}
            <WelcomeGuideAlert {event} />
          {/if}
        </div>

        <hr class="h-px my-2 bg-gray-200 border-0 dark:bg-gray-700" />
        <div class="text-gray-900 font-bold text-xl mb-2">Event Checklist</div>
        <div class="p-2">
          {#each event.checklist || [] as { status, completed, name, description }}
            <TeamChecklistItem status={completed === true ? 'complete' : status}>
              <span slot="title">{name}</span>
              <span slot="description">{description}</span>
            </TeamChecklistItem>
          {/each}
        </div>
        {#if $authTokenParsed?.id === team.team_lead_id}
          <div class="flex items-center">
            {#if !registrationActive(event)}
              <button class="bg-cyber-gray-600 text-white font-semibold py-1 px-2 m-1" disabled>
                <Fa class="inline-block" icon={faLock} />
                <span class="hidden md:inline-block">Registration Locked</span>
              </button>
            {:else if !teamRegisteredForEvent(team, event, eventGroup) && registrationActive(event)}
              <button
                class="bg-cyber-blue-900 text-white font-semibold py-1 px-2 m-1"
                on:click={() => registerTeam(team, event)}>
                <Fa class="inline-block" icon={faUserGroup} />
                <span class="hidden md:inline-block">Register Team</span>
              </button>
            {:else if teamRegisteredForEvent(team, event, eventGroup)}
              <button
                class="bg-red-500 text-white font-semibold py-1 px-2 m-1"
                on:click={() => unregisterTeam(team, event)}>
                <Fa class="inline-block" icon={faUserMinus} />
                <span class="hidden md:inline-block">Unregister Team</span>
              </button>
            {/if}
          </div>
        {/if}
      </div>
    </div>
  {/each}
{/each}
