import * as ENVIRONMENT from '$env/static/public';

export type Environment = {
  GraphQLUrl: string;
  KeycloakClientId: string;
  KeycloakRealm: string;
  KeycloakUrl: string;
  DefaultEventGroupName?: string;
  UmamiTrackingId: string | null;
};

const Env: Environment = {
  GraphQLUrl: ENVIRONMENT.PUBLIC_GRAPHQL_URL,
  KeycloakClientId: ENVIRONMENT.PUBLIC_KEYCLOAK_CLIENT_ID,
  KeycloakRealm: ENVIRONMENT.PUBLIC_KEYCLOAK_REALM,
  KeycloakUrl: ENVIRONMENT.PUBLIC_KEYCLOAK_URL,
  DefaultEventGroupName: ENVIRONMENT.PUBLIC_DEFAULT_EVENT_GROUP_NAME,
  UmamiTrackingId: ENVIRONMENT.PUBLIC_UMAMI_TRACKING_ID || null
};

console.debug('Env', Env);
export default Env;
