import type { LightboxState } from '../interfaces/lightbox-state.interface';

export const lightboxStateDefault: LightboxState = {
  isOpen: false
};
