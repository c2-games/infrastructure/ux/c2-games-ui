import Env from '$lib/Env';

export function getRegistrationUrl(origin?: string) {
  if (!origin) origin = 'https://ncaecybergames.org/';
  if (!origin.endsWith('/')) origin += '/';
  return (
    `${Env.KeycloakUrl}/realms/${Env.KeycloakRealm}/protocol/openid-connect/registrations?` +
    `client_id=${Env.KeycloakClientId}&redirect_uri=${origin}team-dashboard&response_type=code`
  );
}

export const discordInvite = 'https://discord.ncaecybergames.org';

export const discordChannels: Record<string, string> = {
  isThisCompetitionRightForMe: 'https://discord.com/channels/624969095292518401/1206811535390023731',
  mainStage: 'https://discord.com/channels/624969095292518401/1084935327489200268',
  help: 'https://discord.com/channels/624969095292518401/1206812102505926707'
};
