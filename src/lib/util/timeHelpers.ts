// Utility function to format date to local time
export function formatToLocalTime(dateString: Date | null | undefined): string {
  if (!dateString) {
    return 'Invalid date';
  }

  const dateFormat = new Intl.DateTimeFormat('en-US', {
    timeZone: 'America/New_York',
    timeZoneName: 'short',
    year: 'numeric',
    month: 'long',
    day: '2-digit',
    hour: '2-digit',
    minute: '2-digit',
    hour12: true
  });

  return dateFormat.format(dateString);
}
