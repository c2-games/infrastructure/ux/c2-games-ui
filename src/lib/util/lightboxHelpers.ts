import { lightboxState } from '../../stores/lightboxStore';

export const openLightbox = (data = {}) => {
  lightboxState.set({ isOpen: true, data });
};
