import type { LightboxData } from './lightbox-data.interface';

export interface LightboxState {
  isOpen: boolean;
  data?: LightboxData;
}
