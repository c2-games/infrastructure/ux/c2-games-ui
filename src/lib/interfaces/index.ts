import type { DefaultColors } from 'tailwindcss/types/generated/colors';

export type TailwindColors = keyof DefaultColors | 'cyber-blue' | 'cyber-gray';
