---
title: "[]{#_lqssq6oxbppl .anchor}NCAE Cyber Games Rules"
---

# Participant Eligibility

We're so happy to have you participate in our competition! Our mission is to be the first step on your cybersecurity competition journey. It's always fun to win, but the spirit of our competition is growth and education. We have some rules regarding eligibility, but we ask that you please take it upon yourself to be honest and sincere with your skill level and familiarity with events such as this.

Participants and/or teams that are clearly overqualified or do not meet the mission of the competition may be disqualified or converted to an exhibition team. This is at the sole discretion of the competition administration.

Participants must be:

-   Actively enrolled students at the institution they are representing

-   New to cybersecurity competitions

-   Here to learn, grow, and experience an entry-level competition

Participants are not allowed to participate if they:

-   Have participated in two NCAE Cyber Games Competition seasons

-   Participated on a winning team during an NCAE Cyber Games Invitational event

-   Have participated as a team member or alternate to any more advanced competitions

**Note: If you are not sure the NCAE Cyber Games is for you or have any doubts about your eligibility, please reach out to a** **staff member prior to enrolling with a team. NCAE Cyber Games staff can be reached on the [official discord][1] (preferred) or by email at [support@ncaecybergames.org][2].**

# Communication

The NCAE Cyber Games uses Discord as its official communication platform. Information about the competition will be
communicated to you throughout the day, and we encourage you to use your team's dedicated text and voice channels for
communication.

**All participants are required to have an active Discord account linked to their NCAE Cyber Games account, and must connect to the NCAE Cyber Games Discord server.**

Teams that are unresponsive or not registered in the Discord server within 5 days prior to a competition date may be
removed from the competition and placed on a wait-list for that competition date.

# Team Composition

The NCAE Cyber Games is designed to be a team activity. Just like the challenges you will face in the workforce, the challenges in this competition will be difficult to overcome alone. Our goal is to create an environment that allows the highest number of students to participate, which is why we have chosen to allow schools to field multiple teams. We expect all teams to participate independently in the spirit of the competition.

-   Competition teams must consist of at least 3 and no more than 10 participants

-   Institutions may field up to 3 total teams

    -   Separate teams must compete separately and are not allowed to collaborate during the competition in any way

    -   Participants must be distributed across multiple teams as evenly as possible

    -   Participant skill level should be balanced across teams as evenly as possible

    -   Teams from the same institution must participate on the same regional competition date

## Team Captain

The Team Captain represents your team's leadership, and they will be responsible for some additional work including organization and collaboration both internally to your team and with NCAE staff prior to, during, and after the competition.

Each team must include one unique student Team Captain, included as one of the 10 total participants

Team Captains are responsible for:

-   Being the primary point of contact for their team

-   Organizing and distributing important information across their team

-   Identifying a Faculty Liaison (name, email and phone number)

## Faculty Liaison

Each team must identify a part-time or full-time faculty member employed by the institution as a Faculty Liaison. Faculty Liaisons may represent multiple teams from the same institution. Faculty Liaisons can not compete or assist participants during the competition.

Faculty Liaisons are responsible for:

-   Validating the legitimacy of registered teams and participants with competition staff

-   Serving as a point of contact in the event of any escalations before, during, or after a competition

# Competition Conduct

Participants are expected to behave professionally at all times before, during, and after the competition. While we have some specific rules, behavior and conduct during the competition is monitored throughout the competition and anything deemed unprofessional may be acted upon at the discretion of competition staff.

-   Participants are expected to participate in a manner of learning and growth. While some friendly banter and conversation is encouraged, participants found taunting, disrespecting, or acting in a manner deemed inappropriate may be disqualified

-   All NCAE events are drug and alcohol-free events, and no non-prescription drugs or alcohol consumption is permitted at any time during competition hours

-   Protests by any team must be presented in writing by the Team Captain to competition staff as soon as possible. The competition staff will be the final arbitrators for any protests or questions arising before, during, or after the competition.

-   Teams must compete without "outside assistance" from non-team members. Faculty Liaisons and coaches are not team members.

-   Competition challenges are time-intensive to develop and curate, please do not release any walk-throughs, write-ups or any spoilers until the challenges are released publicly by the competition staff.

-   Only public information and resources that could reasonably be available to all teams without charge are permitted.  Any information or resources requiring a membership or fee are not allowed.

-   All custom materials, such as scripts or tools, utilized by a team must be made publicly available on an external site such as Github.  Any custom materials must be disclosed to and approved by competition staff at least one week prior to the competition date.  Please refer to https://gitlab.com/c2-games/blueteam-resources for more information and how to contribute your custom resources.

# Scoring and Advancement

-   Teams may earn points by maintaining the integrity and uptime of their infrastructure services and by completing Capture-The-Flag challenges

-   Teams must not interfere with scoring infrastructure

-   Any action taken by a team or participant that disrupts scoring infrastructure or interferes with the functionality of the scoring engine or manual scoring checks are exclusively the responsibility of the teams

-   Any questions about scoring can be directed at the NCAE Cyber Games staff

-   Advancement to the NCAE National Invitational is granted by competition staff via invitation only. The team with the highest score is not guaranteed an invite. Teams may increase their chances of securing an invite by:

    -   Achieving high Infrastructure and CTF scores

    -   Demonstrating exceptional teamwork and sportsmanship

    -   Demonstrating exceptional growth and learning

  [1]: https://discord.ncaecybergames.org
  [2]: mailto:support@ncaecybergames.org
