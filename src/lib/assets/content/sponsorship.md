The NCAE Cybergames is accepting sponsorship applications for our upcoming 2025 competition season!

Becoming a sponsor is a great way to get involved with the cybersecurity community, and gives you exclusive access to
the next generation of talent.

In 2023, the NCAE Cyber Games saw over **700 participants** from **70+ colleges** and universities across the country,
representing **_170% growth_** over the previous year. In 2024, we saw over **1000 participants** from **100+
institutions**, growing _another 140%_! There’s no better way to instantly reach the ever-growing audience of future
cybersecurity professionals.

Our competition culminates to a national invitational event where you’ll have the opportunity to meet participants
face-to-face from our hand-picked teams from across the country. Sponsorship is accompanied by a shortlist of top
contenders eager for internship opportunities you might be looking to fill with incredible, ambitious students!

Please download our [sponsorship opportunities guide](/NCAE%20Sponsor%20Flyer%202025.pdf) for more information.

We have limited spots this year that will fill up fast! If you have any questions, want to sponsor, or want to be
involved but don’t see a sponsorship opportunity in our guide that fits your idea, please don’t hesitate to reach out to
us!

You may use the form below, or email us directly at [sponsor@ncaecybergames.org](mailto:sponsor@ncaecybergames.org)
